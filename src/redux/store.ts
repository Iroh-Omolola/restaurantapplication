import {createStore, applyMiddleware} from 'redux';
import createSagaMiddleware from 'redux-saga';
import restaurantReducer from './reducer';
import {restaurantSaga} from './saga';

const sagaMiddleware = createSagaMiddleware();
const store = createStore(restaurantReducer, applyMiddleware(sagaMiddleware));

sagaMiddleware.run(restaurantSaga);

export default store;
