import {Restaurant} from '../types';

// action types for all restaurants (get all restaurants)
export const FETCH_RESTAURANTS_REQUEST = 'FETCH_RESTAURANTS_REQUEST';
export const FETCH_RESTAURANTS_SUCCESS = 'FETCH_RESTAURANTS_SUCCESS';
export const FETCH_RESTAURANTS_FAILURE = 'FETCH_RESTAURANTS_FAILURE';

// action types for individual restaurant (get one restaurant)
export const FETCH_RESTAURANT_REQUEST = 'FETCH_RESTAURANT_REQUEST';
export const FETCH_RESTAURANT_SUCCESS = 'FETCH_RESTAURANT_SUCCESS';
export const FETCH_RESTAURANT_FAILURE = 'FETCH_RESTAURANT_FAILURE';



// interfaces for all restaurants (get all restaurants)
export interface FetchRestaurantsRequestAction {
  type: typeof FETCH_RESTAURANTS_REQUEST;
  payload: {
    offset: number;
    limit: number;
  };
}

export interface FetchRestaurantsSuccessAction {
  type: typeof FETCH_RESTAURANTS_SUCCESS;
  payload: {
    restaurants: Restaurant[];
  };
}

export interface FetchRestaurantsFailureAction {
  type: typeof FETCH_RESTAURANTS_FAILURE;
  payload: {
    error: string;
  };
}

// interfaces for individual restaurant (get one restaurant)
export interface FetchRestaurantRequestAction {
  type: typeof FETCH_RESTAURANT_REQUEST;
  payload: {
    _id: string;
  };
}

export interface FetchRestaurantSuccessAction {
  type: typeof FETCH_RESTAURANT_SUCCESS;
  payload: {
  restaurant: Restaurant;
  };
}

export interface FetchRestaurantFailureAction {
  type: typeof FETCH_RESTAURANT_FAILURE;
  payload: {
    error: string;
  };
}

// action creators for  all restaurants (get all restaurants)
export const fetchRestaurantsRequest = (
  offset: number,
  limit: number,
): FetchRestaurantsRequestAction => ({
  type: FETCH_RESTAURANTS_REQUEST,
  payload: {offset, limit},
});

export const fetchRestaurantsSuccess = (
  restaurants: Restaurant[],
): FetchRestaurantsSuccessAction => ({
  type: FETCH_RESTAURANTS_SUCCESS,
  payload: {restaurants},
});

export const fetchRestaurantsFailure = (
  error: string,
): FetchRestaurantsFailureAction => ({
  type: FETCH_RESTAURANTS_FAILURE,
  payload: {error},
});

// action creators for individual restaurant (get one restaurant)
export const fetchRestaurantRequest = (
  _id: string,
): FetchRestaurantRequestAction => ({
  type: FETCH_RESTAURANT_REQUEST,
  payload: {_id},
});

export const fetchRestaurantSuccess = (
  restaurant: Restaurant,
): FetchRestaurantSuccessAction => ({
  type: FETCH_RESTAURANT_SUCCESS,
  payload: {restaurant},
});

export const fetchRestaurantFailure = (
  error: string,
): FetchRestaurantFailureAction => ({
  type: FETCH_RESTAURANT_FAILURE,
  payload: {error},
});

export type RestaurantsAction =
  | FetchRestaurantsRequestAction
  | FetchRestaurantsSuccessAction
  | FetchRestaurantsFailureAction
  | FetchRestaurantRequestAction
  | FetchRestaurantSuccessAction
  | FetchRestaurantFailureAction;
