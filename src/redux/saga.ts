import {put, takeEvery, call} from 'redux-saga/effects';
import axios, {AxiosResponse} from 'axios';

import {
  FETCH_RESTAURANTS_REQUEST,
  fetchRestaurantsSuccess,
  fetchRestaurantsFailure,
  FetchRestaurantsRequestAction,
  FetchRestaurantRequestAction,
  fetchRestaurantSuccess,
  fetchRestaurantFailure,
  FETCH_RESTAURANT_REQUEST,
} from './actions';
import { APP_URL } from '../constant';

// all restaurants
function* fetchRestaurants(action: FetchRestaurantsRequestAction) {
  try {
    const {offset, limit} = action.payload;
    const response: AxiosResponse = yield call(
      axios.get,
      `${APP_URL}`,
      {
        params: {offset, limit},
      },
    );
    yield put(fetchRestaurantsSuccess(response.data.docs));
  } catch (error: any) {
    yield put(fetchRestaurantsFailure(error.message));
  }
}
// individual restaurant
function* fetchRestaurant(action: FetchRestaurantRequestAction) {
  try {
    const {_id} = action.payload;
    const response: AxiosResponse = yield call(
      axios.get,
      `${APP_URL}/${_id}`,
    );
    yield put(fetchRestaurantSuccess(response.data));
  } catch (error: any) {
    yield put(fetchRestaurantFailure(error.message));
  }
}

export function* restaurantSaga() {
  yield takeEvery<FetchRestaurantsRequestAction>(
    FETCH_RESTAURANTS_REQUEST,
    fetchRestaurants,
  );
  yield takeEvery<FetchRestaurantRequestAction>(
    FETCH_RESTAURANT_REQUEST,
    fetchRestaurant,
  );
}
