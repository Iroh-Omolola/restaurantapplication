import {RestaurantsState} from '../types';
import {
  FETCH_RESTAURANTS_REQUEST,
  FETCH_RESTAURANTS_SUCCESS,
  FETCH_RESTAURANTS_FAILURE,
  RestaurantsAction,
  FETCH_RESTAURANT_REQUEST,
  FETCH_RESTAURANT_FAILURE,
  FETCH_RESTAURANT_SUCCESS,
} from './actions';

const initialState: RestaurantsState = {
  loading: false,
  restaurants: [],
  restaurant: {
    _id: "",
    name:"",
    image: {
      url:"",
      file:""
    }
  },
  error: null,
};

const restaurantReducer = (
  state = initialState,
  action: RestaurantsAction,
): RestaurantsState => {
  switch (action.type) {
    case FETCH_RESTAURANTS_REQUEST:
    case FETCH_RESTAURANT_REQUEST:
      return {
        ...state,
        loading: true,
        error: null,
      };
    case FETCH_RESTAURANTS_SUCCESS:
      return {
        ...state,
        loading: false,
        restaurants: [...state.restaurants, ...action.payload.restaurants],
        error: null,
      };
    case FETCH_RESTAURANT_SUCCESS:
      return {
        ...state,
        loading: false,
        restaurant: {...state.restaurant, ...action.payload.restaurant},
        error: null,
      };
    case FETCH_RESTAURANTS_FAILURE:
    case FETCH_RESTAURANT_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload.error,
      };
    default:
      return state;
  }
};

export default restaurantReducer;
