import AsyncStorage from '@react-native-async-storage/async-storage';
import {Restaurant} from './types';
import { ADD_FAVORITE_RESTAURANT } from './constant';

export const getItem = async (key = ADD_FAVORITE_RESTAURANT) => {
  return await AsyncStorage.getItem(key);
};
export const setItem = async (key: string, data: string) =>
  await AsyncStorage.setItem(key, data);

export const addToFavorite = async ({_id}: Restaurant): Promise<string[]> => {
  try {
    const favorites = await getItem(ADD_FAVORITE_RESTAURANT);
    let parsedFavorites: string[] = [...JSON.parse(favorites || '[]')];
    if (parsedFavorites.includes(_id)) {
      parsedFavorites = parsedFavorites.filter(id => id !== _id);
    } else {
      parsedFavorites.push(_id);
    }
    await setItem(ADD_FAVORITE_RESTAURANT, JSON.stringify(parsedFavorites));
    return parsedFavorites as string[];
  } catch (error) {
    console.log({});
    return [];
  }
};
