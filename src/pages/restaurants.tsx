/* eslint-disable react-hooks/exhaustive-deps */
import React, {useEffect, useState} from 'react';
import {Image, SafeAreaView, StyleSheet, TouchableOpacity} from 'react-native';
import {RestaurantList} from '../components/index';
import {useDispatch, useSelector} from 'react-redux';
import {Restaurant, RestaurantsState} from '../types';
import {useFocusEffect, useNavigation} from '@react-navigation/native';
import {ADD_FAVORITE_RESTAURANT} from '../constant';
import {getItem} from '../helper';
import {fetchRestaurantsRequest} from '../redux/actions';

const Restaurants = () => {
  const [favoriteRestaurants, setFavoriteRestaurants] = useState<string[]>([]);
  const [data, setData] = useState<Restaurant[]>([]);
  const navigation = useNavigation();
  const dispatch = useDispatch();

  const {restaurants, loading} = useSelector(
    (state: RestaurantsState) => state,
  );
 const fetchFavoriteRestaurants = async () => {
   try {
     const favorites = await getItem(ADD_FAVORITE_RESTAURANT);
     if (favorites) {
       const parsedFavorites: string[] = JSON.parse(favorites);
       setFavoriteRestaurants(parsedFavorites);
     }
   } catch (error) {
     console.log({});
   }
 };
  useEffect(() => {
    fetchFavoriteRestaurants();
  }, []);

  useEffect(() => {
    setData(() =>
      restaurants.map(restaurant => ({
        ...restaurant,
        isFavorite: favoriteRestaurants.includes(restaurant._id),
      })),
    );
  }, [restaurants, favoriteRestaurants]);

  // Refresh data when returning to the screen
  useFocusEffect(
    React.useCallback(() => {
      dispatch(fetchRestaurantsRequest(0, 10));
      fetchFavoriteRestaurants();
    }, []),
  );


  return (
    <SafeAreaView style={styles.top}>
      <TouchableOpacity
        style={styles.logoContainer}
        activeOpacity={0.5}
        onPress={() => {
          navigation.goBack();
        }}>
        <Image
          alt='back-arrow'
          source={require('../images/back-arrow.png')}
          style={styles.image}
        />
      </TouchableOpacity>
      <RestaurantList restaurants={data} loading={loading} />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  top: {
    paddingTop: 20,
    paddingBottom: 50,
    paddingHorizontal: 10,
    backgroundColor: '#f5f5f5',
  },
  logoContainer: {
    display: 'flex',
    flexDirection: 'column',
    marginBottom: 15,
    width: 100,
  },
  logoContent: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
  },
  logoText: {
    textAlign: 'left',
    paddingLeft: 5,
    fontWeight: 'bold',
    fontSize: 20,
    color: 'orange',
  },
  image: {
    width: 40,
    height: 40,
  },
  text: {
    textAlign: 'center',
    marginVertical: '50%',
    fontSize: 17,
  },
});

export default Restaurants;
