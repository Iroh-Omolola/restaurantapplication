import React from 'react';
import { SafeAreaView, StatusBar, TouchableOpacity} from 'react-native';
import {Image, StyleSheet, Text, View} from 'react-native';

const Home = ({navigation}:any) => {
  return (
    <SafeAreaView style={styles.container}>
      <StatusBar backgroundColor="#f9f7f7" barStyle="dark-content" />
      <View style={styles.top}>
        <Image source={require('../images/logo.png')} style={styles.image} />
        <Text style={styles.logoText}>We Food</Text>
      </View>
      <TouchableOpacity
        style={styles.button}
        activeOpacity={0.5}
        onPress={() => navigation.navigate('Restaurants')}>
        <Text testID="check-restaurants-button" style={styles.text}>
          Check Restaurants
        </Text>
      </TouchableOpacity>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#f9f7f7',
    height:"100%",
    paddingHorizontal:30
  },
  top: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    marginVertical: '33%',
  },
  image: {
    width: 200,
    height: 200,
  },
  logoText: {
    textAlign: 'center',
    paddingLeft: 5,
    fontWeight: 'bold',
    fontSize: 34,
    color: 'orange',
  },
  button: {
    padding:15,
    borderRadius:10,
    backgroundColor:"orange"
  },
  text:{
    color:"white",
    textAlign:"center",
    fontSize:18
  }
});

export default Home;
