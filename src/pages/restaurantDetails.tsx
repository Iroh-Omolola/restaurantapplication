/* eslint-disable react-hooks/exhaustive-deps */
import React, {useEffect} from 'react';
import {Image, SafeAreaView, StyleSheet, TouchableOpacity} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {RestaurantsState} from '../types';
import {LoadingIndicator, RestaurantDetails} from '../components';
import {fetchRestaurantRequest} from '../redux/actions';

const RestaurantDetailsPage = ({navigation, route}: any) => {
  const id = route.params._id;
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchRestaurantRequest(id));
  }, [id]);

  const {restaurant, loading} = useSelector((state: RestaurantsState) => state);

  return (
    <SafeAreaView style={styles.top}>
      <TouchableOpacity
        style={styles.logoContainer}
        activeOpacity={0.5}
        onPress={() => {
          navigation.goBack();
        }}>
        <Image
          alt='back-arrow'
          source={require('../images/back-arrow.png')}
          style={styles.image}
        />
      </TouchableOpacity>
      {loading ? (
        <LoadingIndicator />
      ) : (
        <>
          <RestaurantDetails restaurant={restaurant} />
        </>
      )}
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  top: {
    paddingTop: 20,
    paddingBottom: 50,
    height: '100%',
    paddingHorizontal: 10,
    backgroundColor: '#f5f5f5',
  },
  logoContainer: {
    display: 'flex',
    flexDirection: 'column',
    marginBottom: 15,
    width: 100,
  },
  logoContent: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
  },
  logoText: {
    textAlign: 'left',
    paddingLeft: 5,
    fontWeight: 'bold',
    fontSize: 20,
    color: 'orange',
  },
  image: {
    width: 40,
    height: 40,
  },
});

export default RestaurantDetailsPage;
