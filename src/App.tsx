/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 */
import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Home from './pages/home';
import Restaurants from './pages/restaurants';
import RestaurantDetailsPage from './pages/restaurantDetails';

const Stack = createNativeStackNavigator();

const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={{
          headerShown: false,
        }}>
        <Stack.Screen name="Home" component={Home} />
        <Stack.Screen name="Restaurants" component={Restaurants} />
        <Stack.Screen
          name="RestaurantDetails"
          component={RestaurantDetailsPage}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};



export default App;
