/* eslint-disable react-hooks/exhaustive-deps */
import React, {useCallback, useEffect, useState} from 'react';
import {FlatList, SafeAreaView, StyleSheet, View, VirtualizedList} from 'react-native';
import {useDispatch} from 'react-redux';
import {fetchRestaurantsRequest} from '../redux/actions';
import {Restaurant} from '../types';
import {RestaurantItem, LoadingIndicator} from './index';
import {ParamListBase, useNavigation} from '@react-navigation/native';
import {NativeStackNavigationProp} from '@react-navigation/native-stack';

interface Props {
  restaurants: Restaurant[];
  loading: boolean;
}
export const RestaurantList = ({restaurants, loading}: Props) => {
  const [updateOffset, setUpdateOffset] = useState(0);
  const {navigate} = useNavigation<NativeStackNavigationProp<ParamListBase>>();
  const dispatch = useDispatch();

  const limit = 10;
  const offset = 0;

  useEffect(() => {
    dispatch(fetchRestaurantsRequest(offset, limit));
  }, []);

  const handleNavigateToRestaurantDetails = (e: Restaurant) => {
    navigate('RestaurantDetails', {
      _id: e._id,
    });
  };
  const loadMoreRestaurants = () => {
    if (restaurants.length !== 0) {
      setUpdateOffset(offset + limit);
      dispatch(fetchRestaurantsRequest(updateOffset, limit));
    }
  };

  const keyExtractor = useCallback((item: Restaurant, index: number) => {
    // Added index to the id  to make them unique because an id was identified twice.
    return `${item._id + index + 1}`;
  }, []);

  const onEndReached = useCallback(() => {
    loadMoreRestaurants();
  }, [loadMoreRestaurants]);

  const renderListFooter = () => {
    if (loading) {
      return (
        <View style={styles.loader}>
          <LoadingIndicator />
        </View>
      );
    }
    return null;
  };
  const renderRestaurantItem = ({item}: {item: Restaurant}) => (
    <RestaurantItem
      restaurant={item}
      onClick={() => handleNavigateToRestaurantDetails(item)}
    />
  );

  return (
    <SafeAreaView style={styles.container}>
      <VirtualizedList
        data={restaurants}
        renderItem={renderRestaurantItem}
        keyExtractor={keyExtractor}
        getItemCount={() => restaurants.length}
        getItem={(data, index) => data[index]}
        onEndReached={onEndReached}
        onEndReachedThreshold={0.5}
        ListFooterComponent={renderListFooter()}
      />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#white',
    height: '100%',
  },
  loader: {
    paddingVertical: '50%',
  },
});
