import React, {memo, useEffect, useState} from 'react';
import {View, Text, Image, StyleSheet, TouchableOpacity} from 'react-native';
import {ItemProps} from '../types';
import { ADD_FAVORITE_RESTAURANT } from '../constant';
import { addToFavorite, getItem } from '../helper';

export const RestaurantDetails: React.FC<ItemProps> = memo(({restaurant}) => {
  const {_id, image, name, addressInfo, contacts, cuisines} = restaurant;
  const [favoriteRestaurants, setFavoriteRestaurants] = useState<string[]>([]);

  const toggleFavorite = async () => {
    setFavoriteRestaurants(favoriteRestaurants.filter(id => id !== _id));
    try {
      const parsedFavorites = await addToFavorite(restaurant);
      setFavoriteRestaurants(prev => [...parsedFavorites, ...prev]);
    } catch (error) {
      console.log({});
    }
  };

  useEffect(() => {
    const fetchFavoriteRestaurants = async () => {
      try {
        const favorites = await getItem(ADD_FAVORITE_RESTAURANT);
        if (favorites) {
          const parsedFavorites: string[] = JSON.parse(favorites || '[]');
          setFavoriteRestaurants(parsedFavorites);
        }
      } catch (error) {
        console.log({});
      }
    };
    fetchFavoriteRestaurants();
  }, []);

  return (
    <View style={styles.card}>
      {image && image.url ? (
        <Image source={{uri: image.url}} alt={name} style={styles.image} />
      ) : (
        <View style={styles.textContainer}>
          <Text style={styles.text}>{`${name[0]?.toUpperCase()} ${name[
            name?.length - 1
          ]?.toUpperCase()}`}</Text>
        </View>
      )}
      <View>
        <View style={styles.content}>
          <Text style={styles.title}>Restaurant's Name:</Text>
          <Text style={styles.name}>{name}</Text>
        </View>
        <View style={styles.content}>
          <Text style={styles.title}>Email:</Text>
          <Text style={styles.name}>{contacts?.email}</Text>
        </View>
        <View style={styles.content}>
          <Text style={styles.title}>Address:</Text>
          <Text style={styles.name}>
            {addressInfo?.address} {addressInfo?.city}
          </Text>
        </View>
        <View style={styles.content}>
          <Text style={styles.title}>Country:</Text>
          <Text style={styles.name}>{addressInfo?.country}</Text>
        </View>
        <View style={styles.content}>
          <Text style={styles.title}>Phone:</Text>
          <Text style={styles.name}>{contacts?.phoneNumber}</Text>
        </View>
        {cuisines && cuisines?.length > 0 && (
          <View style={styles.content}>
            <Text style={styles.title}>Cuisines:</Text>
            {cuisines?.map((d, i) => (
              <Text key={d._id} style={styles.name}>
                {d?.name?.en} {i !== cuisines.length - 1 ? ',' : ''}
              </Text>
            ))}
          </View>
        )}
        <View style={styles.content}>
          <Text style={styles.title}>Favorite:</Text>
          <TouchableOpacity onPress={toggleFavorite}>
            {favoriteRestaurants &&
            favoriteRestaurants.length &&
            favoriteRestaurants.includes(_id) ? (
              <Image
                testID="favorite-icon"
                source={require('../images/star.png')}
                style={styles.icon}
              />
            ) : (
              <Image
                testID="favorite-icon"
                source={require('../images/star-1.png')}
                style={styles.icon}
              />
            )}
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
});

const styles = StyleSheet.create({
  card: {
    padding: 10,
    marginTop: 10,
    marginBottom: 16,
  },
  title: {
    fontSize: 17,
    paddingRight: 10,
  },
  image: {
    width: '100%',
    height: 200,
    marginBottom: 8,
    borderRadius: 4,
  },
  icon: {
    width: 20,
    height: 20,
  },
  name: {
    fontSize: 16,
    fontWeight: '400',
    paddingRight: 15,
  },
  textContainer: {
    width: '100%',
    height: 100,
    backgroundColor: 'grey',
    borderRadius: 2,
    padding: 35,
    alignItems: 'center',
    marginRight: 10,
  },
  text: {
    textAlign: 'center',
    color: 'white',
    fontSize: 24,
  },
  content: {
    marginTop: 20,
    display: 'flex',
    flexDirection: 'row',
  },
});
