import React, {useState, memo} from 'react';
import {View, Text, Image, StyleSheet, TouchableOpacity} from 'react-native';
import {ItemProps} from '../types';
import {addToFavorite} from '../helper';

export const RestaurantItem: React.FC<ItemProps> = memo(
  ({restaurant, onClick}) => {
    const [favoriteRestaurants, setFavoriteRestaurants] = useState<string[]>(
      [],
    );
    const {name, _id, image} = restaurant;

    const toggleFavorite = async () => {
      setFavoriteRestaurants(favoriteRestaurants.filter(id => id !== _id));
      try {
        const parsedFavorites = await addToFavorite(restaurant);
        setFavoriteRestaurants(prev => [...parsedFavorites, ...prev]);
      } catch (error) {
        console.log({});
      }
    };

    return (
      <TouchableOpacity onPress={onClick} testID="restaurant-item">
        <View style={styles.card}>
          {image ? (
            <Image source={{uri: image.url}} alt={name} style={styles.image} />
          ) : (
            <View style={styles.textContainer}>
              <Text style={styles.text}>{`${name[0]?.toUpperCase()} ${name[
                name.length - 1
              ]?.toUpperCase()}`}</Text>
            </View>
          )}
          <View style={styles.content}>
            <Text style={styles.name}>{name}</Text>
            <TouchableOpacity onPress={() => toggleFavorite()}>
              {restaurant?.isFavorite || favoriteRestaurants.includes(_id) ? (
                <Image
                  testID="favorite-icon"
                  alt="star-icon"
                  source={require('../images/star.png')}
                  style={styles.icon}
                />
              ) : (
                <Image
                  alt="star-1-icon"
                  testID="favorite-icon"
                  source={require('../images/star-1.png')}
                  style={styles.icon}
                />
              )}
            </TouchableOpacity>
          </View>
        </View>
      </TouchableOpacity>
    );
  },
);

const styles = StyleSheet.create({
  card: {
    backgroundColor: '#f5f3f3',
    borderRadius: 8,
    padding: 16,
    marginBottom: 16,
    elevation: 2,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.2,
    shadowRadius: 2,
  },
  image: {
    width: '100%',
    height: 200,
    marginBottom: 8,
    borderRadius: 4,
  },
  icon: {
    width: 27,
    height: 27,
    marginRight: 10,
  },
  name: {
    fontSize: 16,
    fontWeight: 'bold',
  },
  textContainer: {
    width: '100%',
    height: 100,
    backgroundColor: 'grey',
    borderRadius: 2,
    padding: 35,
    alignItems: 'center',
    marginRight: 10,
    marginBottom: 5,
  },
  text: {
    textAlign: 'center',
    color: 'white',
    fontSize: 24,
  },
  content: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});
