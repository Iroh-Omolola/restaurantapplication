
interface Cuisines {
  _id:string,
  name?: {
    en?: string;
  };
}
export interface Restaurant {
  _id: string;
  name: string;
  image: {
    file: string;
    url: string;
  };
  addressInfo?: {
    address: string;
    city: string;
    country: string;
  };
  contacts?: {
    email: string;
    phoneNumber: string;
  };
  cuisines?:Cuisines[];
   isFavorite?: boolean;
  
}
export interface RestaurantsState {
  loading: boolean;
  restaurants: Restaurant[];
  restaurant: Restaurant;
  error?: string | null;
}

export interface ItemProps {
  restaurant: Restaurant;
  onClick?: any;
}

