import React from 'react';
import {render, waitFor} from '@testing-library/react-native';
import {Provider} from 'react-redux';
import configureStore from 'redux-mock-store';
import { RestaurantList } from '../src/components';
import { fetchRestaurantsRequest } from '../src/redux/actions';

jest.mock('@react-navigation/native', () => ({
  useNavigation: () => ({
    navigate: jest.fn(),
  }),
}));

const mockStore = configureStore([]);

describe('RestaurantList', () => {
  test('fetches restaurants on component mount', async () => {
    const initialState = {restaurants: [], loading: false};
    const store = mockStore(initialState);

    store.dispatch = jest.fn();

    render(
      <Provider store={store}>
        <RestaurantList restaurants={[]} loading={false} />
      </Provider>,
    );

    await waitFor(() =>
      expect(store.dispatch).toHaveBeenCalledWith(
        fetchRestaurantsRequest(0, 10),
      ),
    );
  });

});
