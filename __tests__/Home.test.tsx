import React from 'react';
import {render, fireEvent} from '@testing-library/react-native';
import Home from '../src/pages/home';

describe('Home', () => {
  test('button click navigates to Restaurants screen', () => {
    const navigationMock = {
      navigate: jest.fn(),
    };

    const {getByTestId} = render(<Home navigation={navigationMock} />);

    const button = getByTestId('check-restaurants-button');
    fireEvent.press(button);

    expect(navigationMock.navigate).toHaveBeenCalledWith('Restaurants');
  });
});
