import React from 'react';
import {render, fireEvent} from '@testing-library/react-native';
import { RestaurantItem } from '../src/components';

const mockProps = {
  restaurant: {
    _id: '1',
    name: 'Restaurant Name',
    image: {
        file:"aaaaa.png",
        url: 'https://example.com/image.jpg'
    },
    isFavorite: false,
  },
  onClick: jest.fn(),
};

test('renders restaurant name', () => {
  const {getByText} = render(<RestaurantItem {...mockProps} />);
  const restaurantName = getByText('Restaurant Name');
  expect(restaurantName).toBeDefined();
});

test('calls onClick when pressed', () => {
  const {getByTestId} = render(<RestaurantItem {...mockProps} />);
  const restaurantItem = getByTestId('restaurant-item');
  fireEvent.press(restaurantItem);
  expect(mockProps.onClick).toHaveBeenCalled();
});

