import React from 'react';
import { render, } from '@testing-library/react-native';
import {RestaurantDetails} from '../src/components';
import {ItemProps} from '../src/types';

describe('RestaurantDetails', () => {
  const mockProps: ItemProps = {
    restaurant: {
      _id: '1',
      name: 'Restaurant Name',
      image: {
        file: 'aaaaa.png',
        url: 'https://example.com/image.jpg',
      },
      addressInfo: {
        address: '1 Seaside Estate',
        city: 'Ajah',
        country: 'Nigeria',
      },
      contacts: {
        email: 'test@example.com',
        phoneNumber: '123-456-7890',
      },
      cuisines: [
        {_id: '1', name: {en: 'Cuisine 1'}},
        {_id: '2', name: {en: 'Cuisine 2'}},
        {_id: '3', name: {en: 'Cuisine 3'}},
      ],
    },
  };

  test('renders correctly with props', () => {
    const {getByText, getByTestId} = render(
      <RestaurantDetails {...mockProps} />,
    );

    expect(getByText('Restaurant Name')).toBeDefined();

    expect(getByText('Cuisine 1 ,')).toBeDefined();
    expect(getByText('Cuisine 2 ,')).toBeDefined();
    expect(getByText('Cuisine 3')).toBeDefined();

    expect(getByText('1 Seaside Estate Ajah')).toBeDefined();

    expect(getByText('test@example.com')).toBeDefined();

    expect(getByText('123-456-7890')).toBeDefined();

    expect(getByTestId('favorite-icon')).toBeDefined();
  });

  test('toggles favorite state when favorite icon is pressed', async () => {
    const {getByTestId} = render(<RestaurantDetails {...mockProps} />);

    const favoriteIcon = getByTestId('favorite-icon');

    expect(favoriteIcon.props.source).toEqual({
      testUri: '../../../src/images/star-1.png',
    });
  });
});
